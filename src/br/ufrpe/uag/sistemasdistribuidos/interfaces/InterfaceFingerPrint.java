/**
 * 
 */
package br.ufrpe.uag.sistemasdistribuidos.interfaces;

import java.security.NoSuchAlgorithmException;

/**
 * @author root
 *
 */
public interface InterfaceFingerPrint {

	/**
	 * Implements the algoritm to generate de fingerprint
	 * 
	 * @param bytes
	 * @return
	 * @throws NoSuchAlgorithmException 
	 */
	String getFingerPrint(byte[] bytes) throws NoSuchAlgorithmException;

	/**
	 * Size of print label
	 * @return
	 */
	int getPrintSize();
}

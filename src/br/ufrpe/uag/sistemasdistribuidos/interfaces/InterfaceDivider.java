package br.ufrpe.uag.sistemasdistribuidos.interfaces;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Set;

public interface InterfaceDivider {

	HashMap<String, Set<Integer>> split(String fingerPrint, int sizeInBytes)
			throws NoSuchAlgorithmException, IOException;

	String getPath();

}
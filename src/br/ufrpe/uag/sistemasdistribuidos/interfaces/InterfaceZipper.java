package br.ufrpe.uag.sistemasdistribuidos.interfaces;

import java.io.IOException;
import java.io.InputStream;
import java.security.NoSuchAlgorithmException;

public interface InterfaceZipper {

	// zipper function
	String compress(InputStream inputStream) throws IOException, NoSuchAlgorithmException;
}
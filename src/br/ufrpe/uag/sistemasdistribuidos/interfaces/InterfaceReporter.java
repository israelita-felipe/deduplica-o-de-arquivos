package br.ufrpe.uag.sistemasdistribuidos.interfaces;

import java.io.IOException;

public interface InterfaceReporter {

	/**
	 * Generate a *.csv file with the tests and best block size to use to
	 * deduplicate file.
	 * 
	 * HDDBlockSectorSize can be sad using
	 * "hdparm -I /dev/sda | grep -i physical" in linux terminal as sudo user
	 * 
	 * @param start
	 * @param end
	 * @param HDDBlockSectorSize
	 * @param archives
	 * @throws IOException
	 */

	void reportBlockSize(int start, int end, int HDDBlockSectorSize, String[] archives) throws IOException;

	/**
	 * @return the idealBlockSize
	 */
	int getIdealBlockSize();

	/**
	 * @return the length
	 */
	long getLength();

}
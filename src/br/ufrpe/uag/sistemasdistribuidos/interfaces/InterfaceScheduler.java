package br.ufrpe.uag.sistemasdistribuidos.interfaces;

import java.util.HashMap;
import java.util.Set;

public interface InterfaceScheduler {

	HashMap<String,HashMap<String, Set<Integer>>> process(int blockSize, String[] archives);

	/**
	 * @return the divider
	 */
	InterfaceDivider getDivider();

	/**
	 * @return the zipper
	 */
	InterfaceZipper getZipper();

}
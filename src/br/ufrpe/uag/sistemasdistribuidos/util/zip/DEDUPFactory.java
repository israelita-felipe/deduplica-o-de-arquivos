/**
 * 
 */
package br.ufrpe.uag.sistemasdistribuidos.util.zip;

import java.io.IOException;

import br.ufrpe.uag.sistemasdistribuidos.interfaces.InterfaceDivider;
import br.ufrpe.uag.sistemasdistribuidos.interfaces.InterfaceFingerPrint;
import br.ufrpe.uag.sistemasdistribuidos.interfaces.InterfaceZipper;

/**
 * @author root
 *
 */
public class DEDUPFactory {
	/**
	 * 
	 * @param path
	 * @return
	 */
	public static Divider getDivider(String path) {
		return new Divider(path);
	}

	/**
	 * 
	 * @param path
	 * @param fingerPrint
	 * @return
	 */
	public static Divider getDivider(String path, InterfaceFingerPrint fingerPrint) {
		return new Divider(path, fingerPrint);
	}

	/**
	 * 
	 * @param divider
	 * @param zipper
	 * @return
	 */
	public static Scheduler getScheduler(InterfaceDivider divider, InterfaceZipper zipper) {
		return new Scheduler(divider, zipper);
	}

	/**
	 * 
	 * @param path
	 * @param fingerPrint
	 * @return
	 */
	public static Scheduler getScheduler(String path, InterfaceFingerPrint fingerPrint, int buffer) {
		return new Scheduler(getDivider(path, fingerPrint), getZipper(buffer));
	}

	/**
	 * 
	 * @param path
	 * @param fingerPrint
	 * @param zipFingerPrint
	 * @return
	 */
	public static Scheduler getScheduler(String path, InterfaceFingerPrint fingerPrint,
			InterfaceFingerPrint zipFingerPrint, int buffer) {
		return new Scheduler(getDivider(path, fingerPrint), getZipper(zipFingerPrint,buffer));
	}

	/**
	 * 
	 * @param path
	 * @return
	 */
	public static Scheduler getScheduler(String path, int buffer) {
		return new Scheduler(getDivider(path), getZipper(buffer));
	}

	/**
	 * gets a new reporter stance, and needs to insert the log file name
	 * 
	 * @param fileToLog
	 * @return
	 * @throws IOException
	 */
	public static Reporter getReporter(String fileToLog) throws IOException {
		return new Reporter(fileToLog);
	}

	/**
	 * gets a new zipper instance
	 * 
	 * @return
	 */
	public static Zipper getZipper(int buffer) {
		return new Zipper(buffer);
	}

	/**
	 * 
	 * @param fingerPrint
	 * @return
	 */
	public static Zipper getZipper(InterfaceFingerPrint fingerPrint, int buffer) {
		return new Zipper(fingerPrint, buffer);
	}
}

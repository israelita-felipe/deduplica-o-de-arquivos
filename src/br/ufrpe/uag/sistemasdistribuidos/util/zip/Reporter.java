/**
 * 
 */
package br.ufrpe.uag.sistemasdistribuidos.util.zip;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Set;

import br.ufrpe.uag.sistemasdistribuidos.interfaces.InterfaceReporter;
import br.ufrpe.uag.sistemasdistribuidos.interfaces.InterfaceScheduler;
import br.ufrpe.uag.sistemasdistribuidos.interfaces.InterfaceZipper;
import br.ufrpe.uag.sistemasdistribuidos.util.FileUtil;
import br.ufrpe.uag.sistemasdistribuidos.util.FingerPrint;
import br.ufrpe.uag.sistemasdistribuidos.util.MetaData;

/**
 * @author root
 *
 */
class Reporter implements InterfaceReporter {

	private int idealBlockSize = -1;
	private long length = 0;
	private long metaSize = 0;
	private FileWriter writer = null;
	private String file = null;

	public Reporter(String file) throws IOException {
		// TODO Auto-generated constructor stub
		this.writer = new FileWriter(file);
		this.file = file;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * br.ufrpe.uag.sistemasdistribuidos.util.InterfaceReporter#reportBlockSize(
	 * int, int, int, java.lang.String[])
	 */
	@Override
	public void reportBlockSize(int start, int end, int HDDBlockSectorSize, String[] archives) throws IOException {
		try {
			// current time
			long time = System.currentTimeMillis();
			// compacts
			String[] compactados = new String[archives.length];

			for (int i = 0; i < compactados.length; i++) {
				InterfaceZipper zipperTemp = DEDUPFactory.getZipper(new FingerPrint(), HDDBlockSectorSize);
				compactados[i] = zipperTemp.compress(new FileInputStream(archives[i]));
			}
			for (int i = start; i <= end; i++) {
				// gets a new blocksize
				int blockSize = HDDBlockSectorSize * i;

				// create a scheduler
				InterfaceScheduler s = DEDUPFactory.getScheduler("BLOCKS" + File.separatorChar + blockSize,
						HDDBlockSectorSize);

				long currentTime = System.currentTimeMillis();
				HashMap<String, HashMap<String, Set<Integer>>> hashMap = s.process(blockSize, compactados);
				currentTime = System.currentTimeMillis() - currentTime;

				// tamanho em bytes
				long length = 0;
				for (File f : new File("BLOCKS" + File.separatorChar + blockSize).listFiles()) {
					length += f.length();
				}

				// wite to csv
				writer.write(blockSize + ";" + length + ";" + currentTime + "\n");

				// update de best blocksize
				updateBestBlock(length, blockSize, hashMap, archives);

				// delete tem archive
				FileUtil.deleteDir(new File("BLOCKS" + File.separatorChar + blockSize));
			}
			// write best values
			writer.write("\n");
			writer.write(getIdealBlockSize() + ";" + getLength());
			writer.write("\n");
			writer.write("MetaSize;" + getMetaSize());
			writer.write("\n");
			writer.write("Time;" + (System.currentTimeMillis() - time));

			// close de filewriter
			writer.close();

			for (String compact : compactados) {
				FileUtil.deleteDir(new File(compact));
			}

			System.out.println("Report wrote to " + this.file);
		} catch (IOException ex) {
			writer.close();
			throw new IOException(ex);
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void updateBestBlock(long length, int blockSize, HashMap<String, HashMap<String, Set<Integer>>> hashMap,
			String[] archives) throws IOException {
		// pega o tamanho do metadado
		File meta = MetaData.writeMeta(hashMap, System.currentTimeMillis() + "");
		// pega o tamanho do metadado+o arquivo deduplicado
		long metasize = meta.length() + length;

		// soma os tamanhos dos arquivos originais
		long filesize = 0;
		for (String file : archives) {
			filesize += new File(file).length();
		}
		// atualiza os melhores valores
		if (metasize <= filesize) {

			if (getIdealBlockSize() == -1) {
				setIdealBlockSize(blockSize);
				setLength(length);
				setMetaSize(metasize);
				MetaData.writeMeta(hashMap, this.file);
			} else {
				if (length < getLength()) {
					setIdealBlockSize(blockSize);
					setLength(length);
					setMetaSize(metasize);
					MetaData.writeMeta(hashMap, this.file);
				}
			}
		}
		meta.delete();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.ufrpe.uag.sistemasdistribuidos.util.InterfaceReporter#
	 * getIdealBlockSize()
	 */
	@Override
	public int getIdealBlockSize() {
		return this.idealBlockSize;
	}

	/**
	 * @param idealBlockSize
	 *            the idealBlockSize to set
	 */
	public void setIdealBlockSize(int idealBlockSize) {
		this.idealBlockSize = idealBlockSize;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.ufrpe.uag.sistemasdistribuidos.util.InterfaceReporter#getLength()
	 */
	@Override
	public long getLength() {
		return this.length;
	}

	/**
	 * @param length
	 *            the length to set
	 */
	public void setLength(long length) {
		this.length = length;
	}

	/**
	 * @return the metaSize
	 */
	public long getMetaSize() {
		return metaSize;
	}

	/**
	 * @param metaSize
	 *            the metaSize to set
	 */
	public void setMetaSize(long metaSize) {
		this.metaSize = metaSize;
	}

}

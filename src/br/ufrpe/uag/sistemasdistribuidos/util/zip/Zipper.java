package br.ufrpe.uag.sistemasdistribuidos.util.zip;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import br.ufrpe.uag.sistemasdistribuidos.interfaces.InterfaceFingerPrint;
import br.ufrpe.uag.sistemasdistribuidos.interfaces.InterfaceZipper;
import br.ufrpe.uag.sistemasdistribuidos.util.FingerPrint;

class Zipper implements InterfaceZipper {

	private final int BUFFER_SIZE; 
	private InterfaceFingerPrint fingerPrint;

	public Zipper(int buffer) {
		// TODO Auto-generated constructor stub
		this.BUFFER_SIZE = buffer;
		this.fingerPrint = new FingerPrint();
	}

	public Zipper(InterfaceFingerPrint fingerPrint,int buffer) {
		// TODO Auto-generated constructor stub
		this.BUFFER_SIZE = buffer;
		this.setFingerPrint(fingerPrint);
	}

	public String compress(InputStream inputStream) throws IOException, NoSuchAlgorithmException {
		// Buffer to read the inputStream
		int cont;
		byte[] buffer = new byte[BUFFER_SIZE];

		BufferedInputStream bufferedInputStream = null;
		FileOutputStream fileOutputStream = null;
		ZipOutputStream zipOutputStream = null;
		ZipEntry entry = null;

		// hash from current file
		String hash = getFingerPrint(getFingerPrint(), inputStream);

		fileOutputStream = new FileOutputStream(new File(hash));
		zipOutputStream = new ZipOutputStream(new BufferedOutputStream(fileOutputStream));
		bufferedInputStream = new BufferedInputStream(inputStream, BUFFER_SIZE);
		entry = new ZipEntry("" + hash);
		zipOutputStream.putNextEntry(entry);

		while ((cont = bufferedInputStream.read(buffer, 0, BUFFER_SIZE)) != -1) {
			zipOutputStream.write(buffer, 0, cont);
		}
		bufferedInputStream.close();
		zipOutputStream.close();
		return hash;
	}

	public String getFingerPrint(InterfaceFingerPrint finger, InputStream inputStream) throws NoSuchAlgorithmException {
		return finger.getFingerPrint((inputStream.hashCode() + new Date().toString()).getBytes());
	}

	/**
	 * @return the fingerPrint
	 */
	public InterfaceFingerPrint getFingerPrint() {
		return fingerPrint;
	}

	/**
	 * @param fingerPrint
	 *            the fingerPrint to set
	 */
	public void setFingerPrint(InterfaceFingerPrint fingerPrint) {
		this.fingerPrint = fingerPrint;
	}
	
}
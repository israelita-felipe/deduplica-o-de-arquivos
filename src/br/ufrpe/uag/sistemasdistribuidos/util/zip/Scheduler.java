/**
 * 
 */
package br.ufrpe.uag.sistemasdistribuidos.util.zip;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Set;

import br.ufrpe.uag.sistemasdistribuidos.interfaces.InterfaceDivider;
import br.ufrpe.uag.sistemasdistribuidos.interfaces.InterfaceScheduler;
import br.ufrpe.uag.sistemasdistribuidos.interfaces.InterfaceZipper;

/**
 * @author root
 *
 */
class Scheduler implements InterfaceScheduler {

	private final InterfaceDivider divider;
	private final InterfaceZipper zipper;

	public Scheduler(InterfaceDivider divider, InterfaceZipper zipper) {
		// TODO Auto-generated constructor stub
		this.divider = divider;
		this.zipper = zipper;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * br.ufrpe.uag.sistemasdistribuidos.zip.InterfaceScheduler#process(int,
	 * java.lang.String[])
	 */
	@Override
	public HashMap<String, HashMap<String, Set<Integer>>> process(int blockSize, String[] archives) {
		HashMap<String, HashMap<String, Set<Integer>>> list = new HashMap<>();
		for (String archive : archives) {

			try {

				// split the figerprint file
				list.put(archive, getDivider().split(archive, blockSize));

			} catch (NoSuchAlgorithmException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return list;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * br.ufrpe.uag.sistemasdistribuidos.zip.InterfaceScheduler#getDivider()
	 */
	@Override
	public InterfaceDivider getDivider() {
		return divider;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.ufrpe.uag.sistemasdistribuidos.zip.InterfaceScheduler#getZipper()
	 */
	@Override
	public InterfaceZipper getZipper() {
		return zipper;
	}

}

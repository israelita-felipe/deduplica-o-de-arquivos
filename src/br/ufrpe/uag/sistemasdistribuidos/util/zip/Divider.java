/**
 * 
 */
package br.ufrpe.uag.sistemasdistribuidos.util.zip;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import br.ufrpe.uag.sistemasdistribuidos.interfaces.InterfaceDivider;
import br.ufrpe.uag.sistemasdistribuidos.interfaces.InterfaceFingerPrint;
import br.ufrpe.uag.sistemasdistribuidos.util.FingerPrint;

/**
 * @author root
 *
 */
class Divider implements InterfaceDivider {

	private String path;
	private InterfaceFingerPrint fingerPrint;

	public Divider(String path) {
		if (path != null && !path.trim().toUpperCase().equals("")) {
			this.path = path;
			new File(path).mkdirs();
		}
		this.setFingerPrint(new FingerPrint());
	}

	public Divider(String path, InterfaceFingerPrint fingerPrint) {
		// TODO Auto-generated constructor stub
		this(path);
		this.setFingerPrint(fingerPrint);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * br.ufrpe.uag.sistemasdistribuidos.zip.InterfaceDivider#split(java.lang.
	 * String, int)
	 */
	@Override
	public HashMap<String, Set<Integer>> split(String fingerPrint, int sizeInBytes)
			throws NoSuchAlgorithmException, IOException {
		HashMap<String, Set<Integer>> set = new HashMap<>();

		// Buffer to read the inputStream
		int cont;
		int nblocks = 0;
		byte[] buffer = new byte[sizeInBytes-getFingerPrint().getPrintSize()];

		// buffered input stream to read any size
		BufferedInputStream bufferedInputStream = null;
		// buffered ouput in memory
		ByteArrayOutputStream byteOutputStream = null;

		// load a file from a finger print
		bufferedInputStream = new BufferedInputStream(new FileInputStream(fingerPrint), sizeInBytes-getFingerPrint().getPrintSize());

		while ((cont = bufferedInputStream.read(buffer, 0, sizeInBytes-getFingerPrint().getPrintSize())) != -1) {
			// gets the finger print to the block
			String hash = getFingerPrint().getFingerPrint(buffer);
			// System.out.println(hash);

			// gets if the hash existis
			Set<Integer> currentSet = set.get(hash);
			if (currentSet == null) {
				currentSet = new HashSet<>();				

				File existentFile = new File(path + File.separatorChar + hash);

				if (!existentFile.exists()) {
					
					// writes in memory
					byteOutputStream = new ByteArrayOutputStream();
					byteOutputStream.write(buffer, 0, cont);
					
					//System.out.println("novo bloco");
					// writes to a ouput stream
					FileOutputStream fos = new FileOutputStream(existentFile);
					byteOutputStream.writeTo(fos);
					byteOutputStream.close();
					fos.close();
				}

			}
			// add the hash in the set
			currentSet.add(nblocks);

			// puts the set back
			set.put(hash, currentSet);

			// increments the number of blocks generated
			nblocks++;
		}
		// close the input stream
		bufferedInputStream.close();

		return set;
	}

	/**
	 * @return the path
	 */
	public String getPath() {
		return path;
	}

	/**
	 * @param path
	 *            the path to set
	 */
	public void setPath(String path) {
		this.path = path;
	}

	/**
	 * @return the fingerPrint
	 */
	public InterfaceFingerPrint getFingerPrint() {
		return fingerPrint;
	}

	/**
	 * @param fingerPrint the fingerPrint to set
	 */
	public void setFingerPrint(InterfaceFingerPrint fingerPrint) {
		this.fingerPrint = fingerPrint;
	}
}

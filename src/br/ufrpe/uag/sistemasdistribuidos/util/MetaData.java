/**
 * 
 */
package br.ufrpe.uag.sistemasdistribuidos.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Properties;
import java.util.Set;

/**
 * @author root
 *
 */
public class MetaData {

	/**
	 * 
	 */
	public MetaData() {
		// TODO Auto-generated constructor stub
	}

	public static File writeMeta(HashMap<String, HashMap<String, Set<Integer>>> metaFull, String file)
			throws IOException {
		File f = new File(file + ".mtd");
		Properties p = new Properties();
		if (f.exists()) {
			p.load(new FileInputStream(f));
		}

		for (String keyFull : metaFull.keySet()) {
			HashMap<String, Set<Integer>> hash = metaFull.get(keyFull);

			for (String key : hash.keySet()) {
				String value = p.getProperty(key);

				if (value == null) {
					value = "";
				}

				for (Integer i : hash.get(key)) {
					value = value + String.valueOf(i) + ";";
				}

				p.setProperty(key, value);
			}

		}
		p.store(new FileOutputStream(f), null);
		return f;
	}

}

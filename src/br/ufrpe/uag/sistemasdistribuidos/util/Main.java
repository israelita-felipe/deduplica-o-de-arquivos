/**
 * 
 */
package br.ufrpe.uag.sistemasdistribuidos.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

import br.ufrpe.uag.sistemasdistribuidos.interfaces.InterfaceReporter;
import br.ufrpe.uag.sistemasdistribuidos.util.zip.DEDUPFactory;

/**
 * @author root
 *
 */
public class Main {
	private BufferedReader br;

	public void execute(String archive) throws IOException {
		File block = new File("BLOCKS");
		if (block.exists()) {
			if (FileUtil.deleteDir(block)) {
				System.out.println("BLOCKS Deleted");
			} else {
				System.out.println("Fail to delete BLOCKS");
			}
		}

		FileReader fileReader = new FileReader(new File(archive));
		br = new BufferedReader(fileReader);

		int start = 0;
		int end = 0;
		int HDDBlockSize = 0;
		String line = null;

		// reads start
		line = br.readLine();
		if (line != null) {
			start = Integer.valueOf(line);
		}
		// reads end
		line = br.readLine();
		if (line != null) {
			end = Integer.valueOf(line);
		}
		// reads HDDBlockSize
		line = br.readLine();
		if (line != null) {
			HDDBlockSize = Integer.valueOf(line);
		}

		// if no more lines the readLine() returns null
		while ((line = br.readLine()) != null) {
			// reading lines until the end of the file
			try {
				InterfaceReporter r = DEDUPFactory
						.getReporter("[" + System.currentTimeMillis() + "]" + line.replace("/", "_") + ".csv");
				r.reportBlockSize(start, end, HDDBlockSize, line.split(" "));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		try {
			System.out.println("[start, end, HDDBlockSectorSize] Insert the list file name: ");
			new Main().execute(scan.nextLine());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("All done!");
		scan.close();
		System.exit(0);
	}
}

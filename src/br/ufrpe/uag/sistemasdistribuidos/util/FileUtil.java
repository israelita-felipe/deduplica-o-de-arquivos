/**
 * 
 */
package br.ufrpe.uag.sistemasdistribuidos.util;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * @author root
 *
 */
public class FileUtil {

	/**
	 * 
	 */
	public FileUtil() {
		// TODO Auto-generated constructor stub
	}

	public static void writeFile(String name, long size) throws IOException {
		File f = new File(name);
		FileWriter fw = new FileWriter(f);
		while (f.length() < size) {
			fw.write("X");
		}
		System.out.println("All done!");
		fw.close();
	}

	public static void getSize(String file) throws IOException {
		File f = new File(file);
		FileWriter fw = new FileWriter(f);
		fw.write("");
		fw.close();
		System.out.println(new File(file).length());
	}

	public static boolean deleteDir(File dir) {
		File[] children = dir.listFiles();
		if (children != null) {
			for (File child : children) {
				deleteDir(child);
			}
		}
		return dir.delete();
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			// FileUtil.writeFile("ISR100.txt", 100000000);
			FileUtil.getSize("s.txt");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}

/**
 * 
 */
package br.ufrpe.uag.sistemasdistribuidos.util;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import br.ufrpe.uag.sistemasdistribuidos.interfaces.InterfaceFingerPrint;

/**
 * @author root
 *
 */
public class FingerPrint implements InterfaceFingerPrint {

	/* (non-Javadoc)
	 * @see br.ufrpe.uag.sistemasdistribuidos.interfaces.InterfaceFingerPrint#getFingerPrint(byte[])
	 */
	@Override
	public String getFingerPrint(byte[] bytes) throws NoSuchAlgorithmException {
		MessageDigest md = MessageDigest.getInstance("SHA-512");
		BigInteger hash = new BigInteger(1, md.digest(bytes));
		return hash.toString(16);
	}

	@Override
	public int getPrintSize() {
		// TODO Auto-generated method stub
		return 0;
	}
	
	public static void main(String[] args) {
		try {
			System.out.println(new FingerPrint().getFingerPrint("israel".getBytes()));
			System.out.println(new FingerPrint().getFingerPrint("israel".getBytes()));
			System.out.println(new FingerPrint().getFingerPrint("israel".getBytes()));
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
